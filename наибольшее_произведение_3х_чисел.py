list_of_ints = list(map(int, input().split()))


def maxProductOfThreeNumbers(list_of_ints):

    if len(list_of_ints) < 3:
        raise Exception('Less than 3 items!')

    maxNum = max(list_of_ints[0], list_of_ints[1])
    minNum = min(list_of_ints[0], list_of_ints[1])
    maxProdOf2 = list_of_ints[0] * list_of_ints[1]
    minProdOf2 = list_of_ints[0] * list_of_ints[1]

    maxProdOf3 = list_of_ints[0] * list_of_ints[1] * list_of_ints[2]

    for current in list_of_ints[3:]:

        maxProdOf3 = max(maxProdOf3, current * maxProdOf2, current * minProdOf2)

        maxProdOf2 = max(maxProdOf2, current * maxNum, current * minNum)

        minProdOf2 = min(minProdOf2, current * maxNum, current * minNum)

        maxNum = max(maxNum, current)

        minNum = min(minNum, current)

    return maxProdOf3


print(maxProductOfThreeNumbers(list_of_ints))
